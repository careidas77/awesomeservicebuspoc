﻿namespace Contracts.Commands
{
    using System;

    public interface ICreateOrder
    {
        Guid Id { get; }
        String Product { get; }
    }
}