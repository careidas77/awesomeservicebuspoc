﻿namespace Contracts.Events
{
    using System;

    public interface IOrderCreated
    {
        String Path { get; }
        Guid Id { get; }
    }
}