﻿namespace Messanger1
{
    using System;
    using System.Threading.Tasks;
    using Contracts.Commands;
    using MassTransit;
    using Shared;
    using Shared.Consumers;
    using Shared.ContractImplementations.Commands;
    using static System.Console;

    public class Program
    {
        public static void Main(String[] args)
        {
            Title = "Messenger";

            MainAsync().GetAwaiter().GetResult();
        }

        public static async Task MainAsync()
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(
                configurator =>
                {
                    var host = HostConfiguration.GetStandard(configurator);

                    configurator.ReceiveEndpoint(
                        host,
                        "messenger_queue",
                        e =>
                        {
                            e.Consumer<CreateOrderFaultConsumer>();
                            e.Consumer<OrderCreatedConsumer>();
                        });
                });

            try
            {
                await busControl.StartAsync();

                WriteLine("Press enter to request creation of an order...");
                WriteLine("Press any other key to exit...");

                while (true)
                {
                    var keyInfo = ReadKey();
                    if (keyInfo.Key != ConsoleKey.Enter)
                        break;

                    var productName = ProductGenerator.GetRandomName();
                    var createOrder = new CreateOrder
                    {
                        Id = Guid.NewGuid(),
                        Product = productName
                    };

                    var endPoint = await busControl.GetSendEndpoint(new Uri("rabbitmq://localhost/create_order_queue"));
                    await endPoint.Send<ICreateOrder>(createOrder);

                    await Out.WriteLineAsync($"Create order command issued for {createOrder.Product}.");
                }
            }
            finally
            {
                await busControl.StopAsync();
            }
        }
    }
}