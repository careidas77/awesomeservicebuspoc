﻿using static System.Console;

namespace Notifyer
{
    using System;
    using System.Threading.Tasks;
    using MassTransit;
    using Shared;
    using Shared.Consumers;

    internal class Program
    {
        private static void Main(String[] args)
        {
            Title = "Notifyer";

            MainAsync().GetAwaiter().GetResult();
        }

        private static async Task MainAsync()
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(
                configurator =>
                {
                    var host = HostConfiguration.GetStandard(configurator);
                    configurator.ReceiveEndpoint(
                        host,
                        "notifyer_order_created_queue",
                        e =>
                        {
                            e.Consumer<OrderCreatedConsumer>();
                            e.Consumer<CreateOrderFaultConsumer>();
                        });

                    //configurator.ReceiveEndpoint(host,
                    //    "notifyer_errors_queue",
                    //    e => e.Consumer<CreateOrderFaultConsumer>());
                });

            try
            {
                await busControl.StartAsync();

                await Out.WriteLineAsync("Awaiting orders...");

                ReadLine();
            }
            finally
            {
                await busControl.StopAsync();
            }
        }
    }
}