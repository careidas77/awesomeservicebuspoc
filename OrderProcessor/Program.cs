﻿using static System.Console;

namespace OrderProcessor
{
    using System;
    using System.Threading.Tasks;
    using MassTransit;
    using Shared;
    using Shared.Consumers;

    public class Program
    {
        public static void Main(String[] args)
        {
            Title = "Order Processor";

            MainAsync().GetAwaiter().GetResult();
        }

        public static async Task MainAsync()
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(
                configurator =>
                {
                    var host = HostConfiguration.GetStandard(configurator);

                    configurator.ReceiveEndpoint(
                        host,
                        "order_created_queue",
                        e => { e.Consumer<OrderCreatedConsumer>(); });
                });

            try
            {
                await busControl.StartAsync();

                await Out.WriteLineAsync("Awaiting orders...");

                ReadLine();
            }
            finally
            {
                await busControl.StopAsync();
            }
        }
    }
}