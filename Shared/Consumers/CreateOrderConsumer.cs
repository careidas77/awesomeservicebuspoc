﻿namespace Shared.Consumers
{
    using System.IO;
    using System.Threading.Tasks;
    using Contracts.Commands;
    using Events;
    using MassTransit;
    using static System.Console;

    public class CreateOrderConsumer : IConsumer<ICreateOrder>
    {
        public async Task Consume(ConsumeContext<ICreateOrder> context)
        {
            var order = context.Message;
            var path = $"D:\\Projects\\awesomeservicebuspoc\\Orders\\{order.Id}.txt";
            using (var file = File.CreateText($"{path}"))
                await file.WriteAsync(order.Product);

            await Out.WriteLineAsync($"Order created @ {path}");

            var orderCreated = new OrderCreated
            {
                Id = order.Id,
                Path = path
            };

            await context.Publish(orderCreated);
        }
    }
}