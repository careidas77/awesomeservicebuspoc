﻿namespace Shared.Consumers
{
    using System.Threading.Tasks;
    using Contracts.Commands;
    using MassTransit;
    using static System.Console;

    public class CreateOrderFaultConsumer : IConsumer<Fault<ICreateOrder>>
    {
        public async Task Consume(ConsumeContext<Fault<ICreateOrder>> context)
        {
            var originalMessage = context.Message.Message;
            var exceptions = context.Message.Exceptions;

            await Out.WriteLineAsync($"Order failed: Id = {originalMessage.Id}");
            foreach (var exceptionInfo in exceptions)
            {
                await Out.WriteLineAsync($"\tMessage: {exceptionInfo.Message}");
            }
        }
    }
}