﻿namespace Shared.Consumers
{
    using System.Threading.Tasks;
    using Events;
    using MassTransit;
    using static System.Console;

    public class OrderCreatedConsumer : IConsumer<OrderCreated>
    {
        public async Task Consume(ConsumeContext<OrderCreated> context)
        {
            var order = context.Message;
            await Out.WriteLineAsync("Notifying order created:");
            await Out.WriteLineAsync($"{order.Id} @ {order.Path}");
        }
    }
}