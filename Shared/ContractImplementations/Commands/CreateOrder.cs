﻿namespace Shared.ContractImplementations.Commands
{
    using System;
    using Contracts.Commands;

    public class CreateOrder : ICreateOrder
    {
        public Guid Id { get; set; }
        public String Product { get; set; }
    }
}