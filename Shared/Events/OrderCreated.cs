﻿namespace Shared.Events
{
    using System;
    using Contracts.Events;

    public class OrderCreated : IOrderCreated
    {
        public String Path { get; set; }
        public Guid Id { get; set; }
    }
}