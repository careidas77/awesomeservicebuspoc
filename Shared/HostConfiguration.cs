﻿namespace Shared
{
    using System;
    using Consumers;
    using MassTransit;
    using MassTransit.RabbitMqTransport;

    public class HostConfiguration
    {
        public static IRabbitMqHost GetStandard(IRabbitMqBusFactoryConfigurator configurator)
        {
            var host = configurator.Host(
                new Uri("rabbitmq://localhost/"),
                h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

            return host;
        }

        public static void ConfigureWorker(IRabbitMqHost host, IRabbitMqBusFactoryConfigurator configurator)
        {
            //configurator.UseRetry(Retry.Interval(5, 5000));
            configurator.ReceiveEndpoint(
                host,
                "create_order_queue",
                e =>
                {
                    e.Consumer<CreateOrderConsumer>();
                    e.Consumer<CreateOrderFaultConsumer>();
                });
        }
    }
}