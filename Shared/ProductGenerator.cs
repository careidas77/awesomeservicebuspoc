﻿namespace Shared
{
    using System;
    using Core;

    public class ProductGenerator
    {
        private static readonly String[] Products =
        {
            "Fruit",
            "Bazinga",
            "Car",
            "Airbplane",
            "Guitar",
            "Football",
            "Pencil",
            "Stereo",
            "Book",
            "Comic",
            "TV",
            "Computer",
            "Lawnmover",
            "Paper",
            "Soap",
        };

        public static String GetRandomName()
        {
            var random = new Random();
            return Products[random.Next(0, Products.Length - 1)];
        }
    }
}