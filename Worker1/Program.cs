﻿namespace Worker1
{
    using System;
    using System.Threading.Tasks;
    using MassTransit;
    using Shared;
    using static System.Console;

    public class Program
    {
        public static void Main(String[] args)
        {
            Title = "Worker One";

            MainAsync().GetAwaiter().GetResult();
        }

        public static async Task MainAsync()
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(
                configurator =>
                {
                    var host = HostConfiguration.GetStandard(configurator);
                    HostConfiguration.ConfigureWorker(host, configurator);
                });

            try
            {
                await busControl.StartAsync();

                await Out.WriteLineAsync("Awaiting creation command...");

                ReadLine();
            }
            finally
            {
                await busControl.StopAsync();
            }
        }
    }
}